package Grafos;

import java.util.Arrays;

public class Grafos {
	
	

	private int [][] matAdy;
	//matriz de adyacencia
	private int [][] matAdyPeso;
	//matriz de adyacencia a la que le asignaremos infinito dentro del constructor
	private Vertice [] vertsVec;
	//Vector de vertices el mismo estara lleno de nulos al crear el grafo
	
	
	public Grafos(int cantidadMaximaDeVertices) {
		
		this.matAdyPeso = new int[cantidadMaximaDeVertices][cantidadMaximaDeVertices];
		this.vertsVec = new Vertice[cantidadMaximaDeVertices];
		for(int i=0; i<vertsVec.length; i++)
			vertsVec[i].setNumVertice(i);
		for(int i=0; i<cantidadMaximaDeVertices; i++) {
			for(int j=0; j<cantidadMaximaDeVertices; j++) {
				matAdyPeso[i][j] = 0;}}
		this.matAdy=new int[cantidadMaximaDeVertices][cantidadMaximaDeVertices];
		for(int i=0; i<cantidadMaximaDeVertices; i++) {
			for(int j=0; j<cantidadMaximaDeVertices; j++) {
				matAdy[i][j] = 0;}}
	}
	//el contructor recibe como parametro la cantidad de vertices que va a contener el grafo
	//instancia una matriz de adyacencia de tipo entero, un vector de tipo vertice y le asigna infinito mediante un doble...
	//for a toda la matriz de adyacencia, por ultimo inicializa el contador de vertices en cero.
	
	public void agregarPesoArista(Vertice vertice1, Vertice vertice2, int peso) {
		this.matAdyPeso[vertice1.getNumVertice()][vertice2.getNumVertice()] = peso;
	}
	
	
	public int getIndice(Vertice vertBuscado) {
		int i=0;
		for(i=0;i<vertsVec.length; i++) {
			if(vertBuscado.nomVertice()==vertsVec[i].nomVertice()) {
				return i;
			}
		}	return -1;
	}
	
	//retorna el indice del vertice pasado por parametro, y si no lo encuentra retorna -1 
	
	public boolean siExisteVertice(Vertice vertBuscar) {
		return getIndice(vertBuscar) != -1;
	}
	//determina si el vertice ya existe en el grafo para que si queremos ingresar un vertice a y ya existe no me lo permita

	@Override
	public String toString() {
		String cadena= "";
		int i=0;
		int j=0;
		
		for(i=0;i<matAdyPeso.length;i++) {
			cadena=cadena+ "[";
			for(j=0;j<matAdyPeso.length;i++) {
				cadena=cadena+matAdyPeso[i][j]+ ",";
			}
		}
		return cadena;
	}
	
	//crea una cadena que muestre los elementos del grafo
	
	
	public boolean adyacentes(Vertice a, Vertice b) {
		return this.matAdy[a.getNumVertice()][b.getNumVertice()] == 1;
	}
	//retorna verdadero si los vertices tienen enlace entre si
	
	public void agregarArista(Vertice vertice1, Vertice vertice2) {
		  this.matAdy[vertice1.getNumVertice()][vertice2.getNumVertice()] = 1;
		  this.matAdy[vertice2.getNumVertice()][vertice1.getNumVertice()] = 1;
	}
	
	public void agregarAristaConDireccion(Vertice origen, Vertice destino) {
		  this.matAdy[origen.getNumVertice()][destino.getNumVertice()] = 1;  
	}

	public int[][] getMatAdy() {
		return matAdy;
	}

	public void setMatAdy(int[][] matAdy) {
		this.matAdy = matAdy;
	}

	public int[][] getMatAdyPeso() {
		return matAdyPeso;
	}

	public void setMatAdyPeso(int[][] matAdyPeso) {
		this.matAdyPeso = matAdyPeso;
	}

	public Vertice[] getVertsVec() {
		return vertsVec;
	}

	public void setVertsVec(Vertice[] vertsVec) {
		this.vertsVec = vertsVec;
	}
	
	
	
}